```
________            _________                   
___  __ \__________ ______  /  _______ ________ 
__  /_/ /  _ \  __ `/  __  /   __  __ `__ \  _ \
_  _, _//  __/ /_/ // /_/ /    _  / / / / /  __/
/_/ |_| \___/\__,_/ \__,_/     /_/ /_/ /_/\___/ 
```
<h2>Try it for yourself</h2>

<h3>Part 1</h3>
1. In src there are two files. One with a python file that will fail, and one suggesions of how it will pass. 
2. If you want you code to pass the test, try and edit it in the way written in app_pass.txt

Edit the app_fail.py script in an editor of your choice. If you think it will pass write the following commands in git bash.
1. git status
2. git add .
3. git commit -m "give your commit a message"
4. git push origin your-name

Look at the pipeline again to see if your code passed the test.

<h3>Part 2</h3>
We are going to add some unit tests to our gitlab-ci.yml file. The test are already writte under test/
1. Create another test job. 
2. It will be in the stage test
3. The command that will be run is "python -m unittest discover -s "./tests/"" 
4. If you implemented this, run the git commands again and check if it passed all test.




